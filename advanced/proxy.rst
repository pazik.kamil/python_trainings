Wzorzec projektowy - Proxy/Pełnomocznik
========================================

* ``Klasa proxy`` to klasa która może ograniczać dostęp do następnej klasy

Przypadki użycia
-----------------

* Względy bezpieczeństwa



Diagram UML
------------


Referencje
-----------
* `Derek Benas youtube`_

.. _`Derek Benas youtube`: https://www.youtube.com/watch?v=cHg5bWW4nUI

Dekoratory
==========================

Cel
---

Dekoratory pozwalają rozszerzyć funkcjonalność funkcji lub klasy.
Dektoratory są funkcjami które "opakowują" inne funkcje lub klasy.

Przypadki użycia
----------------------------------


* Pamięć podręczna - (**Cache**),
* Sprawdzanie argumentów,
* Logowanie,
* Rejestracja,
* Weryfikacja


Definicja
----------

.. testcode::

   def decorator_function(origin_function):
       def wraper_function():
           print('przed funkcją o nazwie {}'.format(origin_function.__name__))
           return origin_function()
       return wraper_function

   @decorator_function
   def main_function():
       print('główna funkcja')

   main_function()

.. testoutput::

   przed funkcją o nazwie main_function
   główna funkcja

Definicja gdy funkcja ma parametry (\*args / \**kwargs)
--------------------------------------------------------

.. testcode::

   def decorator_function(origin_function):
       def wraper_function(*args, **kwargs):
           print('przed funkcją o nazwie {}'.format(origin_function.__name__))
           return origin_function(*args, **kwargs)
       return wraper_function


   @decorator_function
   def hello_function(imie, nazwisko):
       print('Czesc jestem {} {}'.format(imie, nazwisko))

   hello_function("Adam", "Nowak")

.. testoutput::

   przed funkcją o nazwie hello_function
   Czesc jestem Adam Nowak


Sparametryzowany dekorator
--------------------------


Przykład
--------

* Cache dla stron w Django cache_page_

.. _cache_page: https://github.com/django/django/blob/3634560fa9ddb342c3a823b78cc63e7000ccabd8/django/views/decorators/cache.py#L8

* Metoda `require http methods`_

.. _`require http methods`: https://github.com/django/django/blob/3634560fa9ddb342c3a823b78cc63e7000ccabd8/django/views/decorators/http.py#L18

.. note::

   ``hasattr`` opisane jest w `Include/abstract.h`_
.. _`Include/abstract.h`: https://github.com/python/cpython/blob/f75d59e1a896115bd52f543a417c665d6edc331f/Include/abstract.h#L43

.. note::

   ``hasattr`` zaimplementowane jest w `Objects/object.c`_
.. _`Objects/object.c`: https://github.com/python/cpython/blob/ab67281e95de1a88c4379a75a547f19a8ba5ec30/Objects/object.c#L987


Użycie
-----------------------------


.. tip::

   podejrzyj plik ``manage.py`` np. poprzez polecenie
   ``cat manage.py``
   można tam znaleźć w jaki sposób django jest uruchamiane

   zmień ustawienia w ``settings.py`` 


.. hint::

   można to zrobić np. poprzez uruchomienie
   ``vim servermonitoring/settings.py``

   albo bezpośredni w **pyCharm**


Zadania
-------

Zero
____



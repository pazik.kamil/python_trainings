Powłoki Python
====================

python
------------------

* domyślnie zainstalowany **shell**

ipython
---------------

* można łatwo doinstalować,
* posiada dodatkowe funkcje,
* koloruje składnie

jupyter-notebook
------------------

* oparty na **ipython**,
* używany najczęściej w zespołach **data science**,
* oferuje dodatkowe funckje jak np. drukowanie projektu z kodem

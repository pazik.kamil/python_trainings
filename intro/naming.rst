Konwencje nazewnicze
=====================

.. contents::

Wcięcia
_______

* Definiują blok podprogramu np. w definicji funkcji, klasy czy bloku **if/else**,
* Nie można mieszać **tabulatora** z **czteroma spacjami**,
* 4 spacje - jako zalecany sposób,
* tabulator jest dozwolony (lecz jeśli zaczynamy lepiej użyć spacji),
* można ustawić środowisko aby **tabulator** był zamieniany na 4 spacje

Systemy notacji
_______________

lower_case_with_underscores
---------------------------

Używane do:

* Nazw funkcji,
* Nazw parametrów,
* Nazw pakietów,
* Nazw modułów,

.. testcode::

   def pole_kwadratu(a, b):
       return a * b

   print(pole_kwadratu(5, 4))

Poniżej wypisane pole kwadratu o bokach 4 i 5 

.. testoutput::

   20

CAPS_WITH_UNDER
---------------
* zmienne globalne,
* stałe zdefiniowane w klasie

>>> PI = 3.14

PascalCase
----------

.. note:: **PascalCase** lub **UpperCamelCase** 
   to styl w którym:

   * Wyrazy pisane są łącznie,
   * Każdy wyraz pisany jest wielką literą,

PascalCase jako nazwa typu danych

>>> from pandas import DataFrame
 

PascalCase użyty do definiowania klasy

.. testcode::

   class SportsCar:
     def __init__(self, name):
         self.name = name

   # Utworzenie obiektu porsch klasy SportsCar
   porsch = SportsCar("porsche")
   print(porsch.name)

Poniżej wypisany atrybut: nazwa 

.. testoutput::

   porsche

.. hint:: Wyjątki powinny być klasą, stąd też pisane są **PascalCase**

Źródła
______

Sprawdź :pep:`8`

.. warning:: Przestrzeganie 79 znaków w linni obecnie może wydawać się nieaktualne

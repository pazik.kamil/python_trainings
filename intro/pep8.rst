Pep8
=====

.. contents::

Użycie lintera
______________

.. code:: bash

   pylint code_file.py

.. image:: pylint.png
   :alt: pylint result

Annotations
------------

.. testcode::

   def dodaj_liczby(a: int, b: int) -> int:
       return a + b

   print(dodaj_liczby(2, 3))

.. testoutput::

   5

Źródła
______

Sprawdź :pep:`8`

.. warning:: Przestrzeganie 79 znaków w linni obecnie może wydawać się nieaktualne

Ide
====================

pyCharm
-------
Płatne środowisko od JetBrains
* Dużo pluginów,
* Wsparcie dla Frameworków,
* Integracja z Docker, bazami danych, konsolą, usługami chmurowymi

.. image:: pycharm.png
   :alt: pycharm screenshot

Visual Studio Code
------------------
* Narzędzie darmowe,
* Trzeba dogrywać pluginy aby rozszerzyć możliwości


Eclipse
-------
* Narzędzie darmowe

Spyder
------
* Narzędzie darmowe
* Używane w celach naukowych lub analizie danych. Mało popularny, wyparty raczej przez **Jupyter Notebook**

IDLE
----
* Narzędzie darmowe,
* Niezalecane,
* Trudny proces developmentu  

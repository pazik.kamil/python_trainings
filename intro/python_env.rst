Środowisko uruchomieniowe
=========================
**Pythona** można uruchomić na kilka sposobów w zależności od zapotrzebowania.

Środowisko wirtualne (venv)
---------------------------
* Odseparowuje środowiska od siebie,
* Rozwiązuje problem konfliktów wersji,
* Pozwala utrzymywać różne wersje **bibliotek/pythona** w naszych projektach 

.. note:: Aby stowrzyć środowisko wirtualne wykonujemy **jedno z dwóch** poleceń
   - ``python -m venv <KATALOG>`` zamiast *KATALOG* najczęściej występuje *venv* lub *env*
   - ``virtualenv <KATALOG>`` np. ``virtualenv venv`` 

.. note:: Uby użyć środowiska musimy wykonać dodatkowe polecenia:
   ``source venv/bin/activate``

.. hint:: W przypadku **Windows** użytkownik aktywuje środowisko bez użycia **source**



Środowisko globalne
-------------------
* Wszystkie paczki są globalne - nie ma separacji,
* Często pojawiają się problemy z zależnościami



Środowisko w kontenerze
-----------------------
* Python dostępny jedynie w kontenerze, 
* Dobre rozwiązanie w przypadku testowania oprogramowania,
* Integralna część obecnych środowisk **CI/CD**

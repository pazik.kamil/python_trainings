Python
====================

Język strukturalny
------------------
* Możliwe jest pisanie prostych skryptów

Język obiektowy
---------------
* Wszystko jest obiektem


Język funkcyjny
---------------

>>> liczby = [1, 2, 3, 4, 5]
>>> potegi_dwojki = [2**n for n in liczby]
>>> potegi_dwojki
[2, 4, 8, 16, 32]

Dynamicznie typowany
---------------------
* Typy określane są w trakcie wykonania programu,
* Z jednej strony swoboda, z drugiej wolniejsze oprogramowanie,
* Brak kompilacji - błędy związane ze złym typem pojawiają się dopiero po uruchomieniu wadliwej linni kodu  

.. testcode::

   miejsce = 43 # int
   miejsce = "przy oknie" # str
   print(miejsce)

W wyniku zostanie nadpisana zmienna (również typ się zmieni)

.. testoutput::

   przy oknie

Garbage collector
-----------------
* Zarządza oczyszczaniem pamięci,
* Oparty na algorytmie zliczającym ilość **referencji** na dany obiekt

Przekazywanie wartości przez referencje
---------------------------------------


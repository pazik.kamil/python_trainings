Instalacja
====================

Windows
-------
* Instalator exe dostępny na stronie_.

.. _stronie: https://www.python.org/downloads/windows/

Mac
---
* Najlepiej poprzez instalację z brew,
* Ewentualnie przy użyciu instalatora ze strony_

.. _strony: https://www.python.org/downloads/mac-osx/

Linux
-----
* Na **debianie** lub **ubuntu** ``sudo apt-get update && sudo apt-get install python python3-pip python3-venv``
* Na **fedorze** ``sudo dnf install python`` albo konkretna wersja ``sudo dnf install python37``

.. hint:: Aby sprawdzić używaną wersję pythona wykonujemy polecenie: ``python --version`` lub ``python -V``

Kod źródłowy
------------
* Klon repozytorium z github_

.. _github: https://github.com/python/cpython



Przedmowa
====================

Geneza języka
----------------

* Open Source,
* Nazwa języka pochodzi od serialu BBC Latający Cyrk Monty Pythona. Twórca jest fanem serialu,
* Python (jego interpreter) zaczął powstawać od 1989,
* Guido van Rossum - były dobrotliwy, dożywotni dyktator. Jest jak Linus Torvalds dla kernela Linuxa,
* Potrzeba było łatwe tworzenie narzędzi systemowych do systemu operacyjnego Amoeba,
* Coś co byłoby pomiędzy językiem C a Shell,
* Python 2.0 – październik 2000,
* Python 3.0 - grudzień 2008


Zastosowanie
----------------

* DevOps

  * Boto3,
  * Redhat - narzędzia,

    * Instalator **Anaconda**,
    * system-config-network-tui,
    * system-config-services,
    * inne *system-config-*,
    * instalatory paczek (**yum - python2, dnf - python3**),
    * OpenStack,
    * Ansible ( zarządzanie konfiguracją / wdrożeniem )

* Data Science / Machine Learning

  * sklearn,
  * Tensorflow,
  * pySpark
    
* Web Development

  * Django,
  * Flask
  

Kto używa
---------

- Google – jako główny język w firmie, obok jest Java oraz C++. Do przetwarzania ogromnej ilości danych od użytkowników,
- Netflix – do skalowania infrastruktury, alerty w przypadku zmiany ustawień zabezpieczeń,
- Instagram (framework Django), Facebook (Framework Tornado),
- Spotify (Duży wolumen danych do przeprocesowania – Luigi),
- Nasa


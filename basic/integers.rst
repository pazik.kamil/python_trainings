Liczby całkowite
================


Definiowanie
------------

>>> wynagrodzenie_netto = 8000
>>> print(wynagrodzenie_netto)
8000


Sprawdzenie typu
----------------
>>> type(wynagrodzenie_netto) 
<class 'int'>

Przyrównanie typów: string oraz integer

>>> wynagrodzenie_str = '8000'
>>> wynagrodzenie_str == wynagrodzenie_netto
False

Konwersja typów
---------------
>>> wynagrodzenie_converted = int(wynagrodzenie_str)
>>> wynagrodzenie_converted == wynagrodzenie_netto
True

Operacje na liczbach
--------------------

Po podwyżce dostajemy o 5% więcej pieniędzy

>>> wynagrodzenie_netto = wynagrodzenie_netto*1.05
>>> print(wynagrodzenie_netto)
8400.0

>>> type(wynagrodzenie_netto) == float
True

>>> type(wynagrodzenie_netto)
<class 'float'>

Dodawanie
- dostaliśmy miesięczny bonus 200 zł

>>> wynagrodzenie_netto += 200
>>> wynagrodzenie_netto = int(wynagrodzenie_netto)
>>> print(wynagrodzenie_netto)
8600

Dzielenie całkowito liczbowe
- Chcemy policzyć jaki jest dochód netto na osobę w 3 osobowej rodzienie,
- Przyjmniej dokładnośc do drugiego miejsca po przecinku (zaokrąglenie)

>>> print(round(wynagrodzenie_netto / 3, 2))
2866.67

>>> wynagrodzenie_netto // 3
2866

Jak widzimy straciliśmy precyzję. Wartości po przecinku zostały zignorowane


Dzielenie modulo
- Sprawdzenie czy nasza kwota jest liczbą parzystą

>>> print(wynagrodzenie_netto % 2)
0

Nie ma reszty z dzielenia - więc liczba jest parzysta

Ciągi znaków
====================

Wypisywanie ciągów znaków
--------------------------

>>> print('Hello World!')
Hello World!

albo w innej formie cudzysłowów
>>> print("Hello World!")
Hello World!

Definiowanie
------------------


.. testcode::

   tekst = "Hello World!"
   print(tekst)

.. testoutput::

   Hello World! 

Sprawdzenie typu
----------------
>>> type(tekst)
<class 'str'>

Sprawdzenie długości napisu
---------------------------
>>> len(tekst)
12


Wyświetlanie specjalnych znaków
-------------------------------
* Znak nowej linni

>>> print("Hello\nWorld!")
Hello
World!

* Znak tabulatora

>>> print("Hello\tWorld!") # doctest: +SKIP


Konkantenacja znaków
---------------------

>>> print('Hello ' +  'uczestniku') 
Hello uczestniku

Konkantenacja znaków - format
------------------------------

>>> print('Hello {}, have a great day'.format('Tomasz'))
Hello Tomasz, have a great day


Różne reprezentacje - format
-----------------------------

>>> '{:s}'.format('Ciąg znaków') # w przypadku podania liczby zamiast stringa - wyjątek
'Ciąg znaków'

.. testcode::

   class Data:
       """Simple Data class"""
       def __str__(self):
           return 'str'

       def __repr__(self):
           return 'repr'


   print("{0!s} {0!r}".format(Data(), Data()))
   print("{obiekt!s} {obiekt!r}".format(obiekt=Data()))

.. testoutput::

   str repr
   str repr

>>> '{:>10}'.format('test')
'      test'

>>> '{:10}'.format('test')
'test      '

>>> '{:^10}'.format('test')
'   test   '

Funkcje dostępne na stringach
------------------------------

>>> 'Hello'.endswith('o')
True

>>> 'Hello'[-1] == 'o'
True

Podciągi w ciągach znaków
--------------------------
>>> 'Hello'[-1]
'o'

>>> 'Hello'[0:6:2]
'Hlo'

.. testcode::

  imiona = 'Marta, Kasia, Monika, Tomek, Przemek, Janek, Marta, Malgosia'
  
  print(imiona.count('Ma'))

W wyniku dostajemy ilość wystąpień ciągu 

.. testoutput::

  3

>>> imiona.find('Kasia')
7

Dzielenie ciagów po danym seperatorze
--------------------------------------

>>> imiona.split(',')
['Marta', ' Kasia', ' Monika', ' Tomek', ' Przemek', ' Janek', ' Marta', ' Malgosia']

Otrzymaliśmy listę ciągów

Operacje na stringach
---------------------

>>> imiona = imiona.replace("Janek", "Adam")
>>> print(imiona)
Marta, Kasia, Monika, Tomek, Przemek, Adam, Marta, Malgosia

Sprawdzenie czy ciąg jest liczbą

>>> imiona.isdigit()
False

>>> temperatura = "34"
>>> print(temperatura.isdigit())
True

Pisanie ciągu z wielkich liter

>>> print(imiona.upper())
MARTA, KASIA, MONIKA, TOMEK, PRZEMEK, ADAM, MARTA, MALGOSIA

Pisanie ciągu z małych liter

>>> print(imiona.lower())
marta, kasia, monika, tomek, przemek, adam, marta, malgosia

Zadania
-------
* Stwórz program wypisujący twoje imienie i nazwisko,
* Stwórz kod wypisujący: "Test znaków: ', /, " "
* Stwórz dwóch uczestników szkolenia *( wybierz dowolne imiona i przypisz do osobnych zmiennych)*

  * pierwszy_uczestnik,
  * drugi uczestnik

* Zamień uczestników miejscami
  
  * Wypisz uczestników,
  * Czy jest możliwy inny sposób na zamianę miejsc ?

* Niech użytkownik podaje swoje imie przy każdym uruchomieniu kodu **(Użyj google)**

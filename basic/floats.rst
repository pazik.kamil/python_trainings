Liczby zmienno przecinkowe
==========================


Definiowanie
------------

>>> wynagrodzenie_netto = 8000.63
>>> print(wynagrodzenie_netto)
8000.63


Sprawdzenie typu
----------------
>>> type(wynagrodzenie_netto) 
<class 'float'>


Konwersja typów
---------------
>>> wynagrodzenie_converted = int(wynagrodzenie_netto)
>>> wynagrodzenie_converted == wynagrodzenie_netto
False

Ciekawostki
------------

>>> print(0.1 + 0.2)
0.30000000000000004

.. hint::

   Podpowiedz na stronie_.

   .. _stronie: https://0.30000000000000004.com/

   Dodatkowo wpis na wikipedi_.

   .. _wikipedi: https://pl.wikipedia.org/wiki/IEEE_754

   .. code-block:: python

   import decimal 

   ctx = decimal.getcontext()
   print(ctx)

   a = decimal.Decimal(0.2)
   b = decimal.Decimal(0.1)

   ctx.prec = 6
   print(a + b)

   



Zadania
_______
- Oblicz sumę liczb 123, 321, 675 oraz wyświetl ją na ekranie,
- Sprawdz czy suma ta jest wielokrotnością liczby 5,
- Oblicz należny podatek (linniowy 19%) od kwoty podanej przez użytkownika (input). Załóż że kwota wolna od podatków to 5000 
- oblicz pole koła (o podanym promieniu przez użytkownika), zaimportuj dodatkowy moduł **google**

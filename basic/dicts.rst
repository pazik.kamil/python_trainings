Słowniki
==========================
Typ danych **klucz - wartość**

Definiowanie
------------

>>> pracownicy = {1: 'Adam', 3: 'Tomasz', 4: 'Kasia'}
>>> print(pracownicy) # doctest: +SKIP

Sprawdzenie typu
----------------

>>> type(pracownicy)
<class 'dict'>

Operacje na slownikach
-----------------------

* Sprawdzenie długości słownika

>>> len(pracownicy)
3

* Sprawdzanie wystąpienia elementu

>>> 3000 in pracownicy
False

>>> 1 in pracownicy
True

Pracownik o id 1 znajduje sie w slowniku

* Dodanie elementu do listy

>>> pracownicy[15] = "Marek"
>>> print(pracownicy) # doctest: +SKIP
>>> print(len(pracownicy))
4

Zadania
-------

Pierwsze
________

- Utwórz słownik ze stolicami:

  - Francji, Niemiec, Polski, Czech
  - Sprawdz stolicę Uk - w przypadku braku wyświetl "stolica nieznana" **google**
  - Dodaj stolicę Uk - Jeśli brakuje
  - Usuń stolicę Czech ze słownika

Listy
==========================


Definiowanie
------------

>>> lista_plac = [4000, 5000, 3000, 8000]
>>> print(lista_plac)
[4000, 5000, 3000, 8000]


Sprawdzenie typu
----------------

>>> type(lista_plac)
<class 'list'>

Operacje na listach
-------------------

* Sprawdzenie długości listy

>>> len(lista_plac)
4

* Sprawdzanie wystąpienia elementu

>>> 3000 in lista_plac
True

* Dodanie elementu do listy


.. testcode::

   lista_plac.append(12000)
   print(lista_plac)

.. testoutput::

   [4000, 5000, 3000, 8000, 12000]


* Wstawienei elementu na konkretną pozycję listy

.. testcode::

   lista_plac.insert(1, 4500)
   print(lista_plac)

.. testoutput::

   [4000, 4500, 5000, 3000, 8000, 12000]

* Posortowanie listy

.. testcode::

   lista_plac = sorted(lista_plac)
   print(lista_plac)

.. testoutput::

   [3000, 4000, 4500, 5000, 8000, 12000] 


.. testcode::

   print(sorted(lista_plac, reverse=True))

.. testoutput::

   [12000, 8000, 5000, 4500, 4000, 3000]

.. testcode::

   lista_plac.extend([2800, 15000])
   lista_plac.sort()
   print(lista_plac)

.. testoutput::

   [2800, 3000, 4000, 4500, 5000, 8000, 12000, 15000]

Wyciąganie ostatniego elementu z listy

>>> wydobyty = lista_plac.pop() 
>>> print(wydobyty)
15000

Ponownie "dorzucimy" element do listy

>>> lista_plac.append(wydobyty)
>>> print(lista_plac)
[2800, 3000, 4000, 4500, 5000, 8000, 12000, 15000]

Dorzucimy też element który jest niepoprawny (ujemne wynagrodzenie)

>>> lista_plac.append(-300)
>>> print(lista_plac)
[2800, 3000, 4000, 4500, 5000, 8000, 12000, 15000, -300]

Taki element jest nam niepotrzebny - usuniemy go:

>>> del lista_plac[-1]
>>> print(lista_plac)
[2800, 3000, 4000, 4500, 5000, 8000, 12000, 15000]



Konwersja typów
---------------
- to tupli

.. testcode::

   tupla_plac = tuple(lista_plac)
   print(tupla_plac)

.. testoutput::

   (2800, 3000, 4000, 4500, 5000, 8000, 12000, 15000)

- do set-a


>>> set_plac = set(lista_plac)
>>> print(set_plac)  # doctest: +SKIP



Zadania
-------

Pierwsze
________

- Stwórz listę składających się z elementów:
- A = „audi”
- B = „bmw”
- C = „mercedes”
- D = „mazda”
- Zamień Audi z Mazdą miejscami
- Usuń ostatni samochód
- Wypisz ostatni element z listy

Drugie
________

- Utwórz listę temperatu ``[-5, -4, 0, -3, -2, 9, 10]``,
- Posortuj od największej,
- Przeiteruj listę od tyłu,
- Czy jest jakaś różnica ?

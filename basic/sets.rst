Sety
==========================
Typ danych - tak jak zbióry w matematyce

Definiowanie
------------

>>> A = {1, 2, 3, 4, 5}
>>> B = {4, 5, 6, 7, 8}

Sprawdzenie typu
----------------

>>> type(A)
<class 'set'>

Operacje na zbiorach
----------------------

* Sprawdzenie długości zbioru

>>> len(A)
5

* Sprawdzanie wystąpienia elementu

>>> 17 in A
False

>>> 3 in A
True

* Dodanie elementu do listy

>>> A.add(17)
>>> 17 in A
True

* Unia

>>> C = A | B
>>> print(C.issuperset(A))
True

>>> print(C.issuperset(A))
True

* Część wspólna

>>> D = A & B
>>> print(D) # doctest: +SKIP

* Różnica

>>> E = A - B # doctest: +SKIP
>>> F = B - A # doctest: +SKIP

* Różnica symetryczna

>>> print(A.symmetric_difference(B)) # doctest: +SKIP

Zbiory imutowalne
-----------------

>>> A = frozenset([1, 2, 3, 4])

Implementacja
--------------

Kod w C do podejrzenia_

.. _podejrzenia: https://github.com/python/cpython/blob/6673decfa0fb078f60587f5cb5e98460eea137c2/Objects/setobject.c#L1805

Zadania
-------

Pierwsze
________

- mając biory:

  - A = {'wp.pl', 'onet.pl', 'google.com', 'ing.pl', 'facebook.com'}
  - B = {'wp.pl', 'youtube.pl', 'wikipedia.org', 'ovh.com', 'facebook.com'}

- Znajdz:

  - Te domeny które w obu zbirach są wspolne
  - Domeny występującę wyłącznie albo w jednym albo w drugim zbiorze 

Drugie
________

- mając listę [1, 2, 4, 5, 7, 7, 7] wyświetl jedynie jej unikatowe wartości

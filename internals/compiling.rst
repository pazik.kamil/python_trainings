Pozyskanie kodu
====================

* Utworzenie forka_ na githubie

.. _forka: https://help.github.com/articles/fork-a-repo/

* Sklonowanie_ lokalnie repozytorium

.. _Sklonowanie: https://help.github.com/articles/cloning-a-repository/

* Dodanie upstream remote_ 

.. _remote: https://help.github.com/articles/adding-a-remote/


Kompilacja
====================


.. hint::

   Najlepiej skompilować z opcjami ``tls/ssl``

   W przeciwnym wypadku ``pip`` będzię zgłaszał poniższy problem.
   ``Can't connect to HTTPS URL because the SSL module is not available``


Poniżej polecenie do konfigurowania oraz kompilacji

.. code:: bash

   ./configure --with-pydebug --with-openssl=$(brew --prefix openssl) && make -j
   # Kompilacja

   make -j2 # 2 joby


   
.. hint::

   Parametr ``-j`` określa ilość "jobów" odpalonych dla kompilacji.
   Więc możemy uzyć takiej kombinacji na ``Macu``
   ``make -j$(sysctl hw.ncpu | grep -Eo "\d+")``


Aby używać tymczasowo naszego skompilowanego ``pythona`` musimy wskazać go w ścieżce path

.. code:: bash

   PATH="/usr/local/bin:$PATH"
   export PATH

Po tej operacji ``python3`` wskazywać będzie na naszą skompilowaną wersję 

.. caution::

   Zmienna ``PATH`` będzie zmieniona jedynie w trakcjie sesji terminala. 

Instalacja
-----------

.. code:: bash

   make altinstall

.. hint::

   Aby zaistalować ``python3`` na stałe musimy zrobić ``make install`` zamiast ``altinstall``


Wartości predefiniowane - Makra kompilatora
--------------------------------------------

Wyświetlenie wszystkich predefiniowanych wartości

.. code:: bash

   gcc -dM -E - </dev/null

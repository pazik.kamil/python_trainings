python3 range vs python2 range vs python2 xrange
=================================================

.. code-block:: python

   9999999999999999999 - 21 in range(1000000000, 9999999999999999999, 3)
   # w python 3 szybkie

Python 3 bierze pod uwagę __start__, __stop__ oraz __step__

.. code-block:: python

   9999999999999999999 - 21 in xrange(1000000000, 9999999999999999999, 3)
   # w python 2 wolne


analiza kodu
-------------

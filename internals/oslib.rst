Zmiany w module os
====================

Powiedzmy, że chcielibyśmy zmienić zachowanie metod z modułu ``os``.

Założenia
---------

* ``listdir`` będzie pokazywał pliki/foldery które nie są ukryte (Wszystkie pliki/foldery z **kropką** na początku będą pomijane) 

Utworzenie gałęzi (branch)
--------------------------
Aby utworzyć gałąż z naszymi zmianami:


.. code:: bash

   git checkout -b oslib_changes


Uruchomienie testów
--------------------
Aby upewnić się, że wszystkie testy przechodzą.

.. code:: bash

   make test

Wyszukiwanie kodu
------------------

Domyślnie moduły znajdują się w: ``Lib/``. Do `podejrzenia na github`_

.. _`podejrzenia na github`: https://github.com/python/cpython/blob/master/Lib/os.py

Jako że używamy systemu ``posix`` (mac). W kodzie mamy `sprawdzenie tego faktu`_ w linni 48 ``os.py``. 

.. _`sprawdzenie tego faktu`: https://github.com/python/cpython/blob/e75eeb00b56b45261a8b94748066f3b855e06353/Lib/os.py#L48

* Linia 48 - sprawdzenie systemu
* Linia 51 - załadowanie wszystkiego z ``posix``

.. code-block:: python
   :linenos:
   :lineno-start: 48
   :emphasize-lines: 1, 4 

   if 'posix' in _names:
    name = 'posix'
    linesep = '\n'
    from posix import *
    try:
        from posix import _exit
        __all__.append('_exit')
    except ImportError:
        pass
    import posixpath as path

    try:
        from posix import _have_functions
    except ImportError:
        pass

    import posix
    __all__.extend(_get_exports_list(posix))
    del posix

* Po wyszukaniu ``listdir`` niczego nie znaleziono,
* Prawdopodobnie ``listdir`` jest w innym module - np. ``posix``.

.. hint:: 

   Biblioteka ``posix`` jest napisana w niskopoziomowy sposób **język C**
   Jego prawdziwa nazwa to:
   `posixmodule.c`_

.. _`posixmodule.c`: https://github.com/python/cpython/blob/e75eeb00b56b45261a8b94748066f3b855e06353/Modules/posixmodule.c#L3574   

* Linia 2 - nazwa funkcji dostępna w **debugerze**,
* Linia 8 - sprawdzenie czy jest dostępna funkcja ``fdopendir`` - funkcja Unixowa

.. hint:: 

   Można sprawdzić tą funkcję przy użyciu ``man fdeopendir``

.. code-block:: c
   :linenos:
   :lineno-start: 3574
   :emphasize-lines: 1:2,8

   static PyObject *
   _posix_listdir(path_t *path, PyObject *list)
   {
       PyObject *v;
       DIR *dirp = NULL;
       struct dirent *ep;
       int return_str; /* if false, return bytes */
       #ifdef HAVE_FDOPENDIR
       int fd = -1;
       #endif

       errno = 0;
       #ifdef HAVE_FDOPENDIR
       if (path->fd != -1) {
       /* closedir() closes the FD, so we duplicate it */
           fd = _Py_dup(path->fd);
           if (fd == -1)
               return NULL;

Zadania
--------
* Zmienić

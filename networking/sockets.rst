Gniazda / sockety
==================


Adres IP
---------


.. hint::

   biblioteka wbudowana ipaddress_

   .. _ipaddress: https://github.com/python/cpython/blob/0b9bd5b4c31f4bfcd7bbdc13b5ebc54ce0dbd0c1/Lib/ipaddress.py#L1262
   

.. hint::

   Jak sprawdzić nasłuchujące porty ?

   ``netstat -nap tcp | grep -i "listen"``



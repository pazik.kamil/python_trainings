Web api - Django
==========================

Stworzenie środowiska wirtualnego
----------------------------------

.. code-block:: bash

   python -m venv venv
   source venv/bin/activate

Instalacja
----------

.. code-block:: bash

   pip install django
   pip install djangorestframework # pamietaj o dodaniu 'rest_framework', do settings.py - INSTALLED_APPS
   pip install django-extensions # pamietaj o dodaniu 'django_extensions', do settings.py - INSTALLED_APPS
   pip install markdown       # Markdown support for the browsable API.
   pip install django-filter  # Filtering support - pamietaj o dodaniu do settings.py 'django_filters' - INSTALLED_APPS

Utworzenie pliku zależności   
-----------------------------

.. code-block:: bash

   pip freeze > requirements.txt

Utworzenie nowego projektu
--------------------------

.. code-block:: bash

   django-admin startproject servermonitoring
   cd servermonitoring
   python manage.py migrate
   python manage.py runserver

Utworzenie nowej aplikacji
--------------------------

.. code-block:: bash

  django-admin startapp api

Sprawdzenie w przeglądarce
---------------------------

* Chrome/Postman pod adres ``http://127.0.0.1:8000/``
* Curl ``http://127.0.0.1:8000/``

Dostosowanie ustawień
----------------------

.. tip::

   podejrzyj plik ``manage.py`` np. poprzez polecenie
   ``cat manage.py``
   można tam znaleźć w jaki sposób django jest uruchamiane

   zmień ustawienia w ``settings.py`` 

.. code-block:: bash

   vim servermonitoring/settings.py

.. hint::

   można to zrobić np. poprzez uruchomienie
   ``vim servermonitoring/settings.py``

   albo bezpośredni w **pyCharm**

.. code-block:: bash

   # servermonitoring/settings.py

   INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_filters',
    'django_extensions',
    'rest_framework',
    'api'
   ]

Utworzenie testów
-----------------

.. code-block:: python

   # api/tests.py

   from django.test import TestCase
   from rest_framework import status

   from api.models import Server

   class ServerModelTestCase(TestCase):
       """Klasa testów modelu Server"""

       def setUp(self):
           """Definicja wartosci startowych"""

           self.server = Server(address='127.0.0.1')

       def test_model_repr(self):
           self.assertEqual("<Server address: 127.0.0.1>", repr(self.server))

       def test_model_str(self):
           self.assertEqual("Server o adresie: 127.0.0.1", str(self.server))

Odpalenie testu
---------------

.. code-block:: bash

   python manage.py test


Tworzenie modeli
-----------------

.. code-block:: python

   from django.db import models

   class Server(models.Model):
       created = models.DateTimeField(auto_now_add=True)
       location = models.CharField(max_length=100, null=True, blank=True, default='')
       available = models.BooleanField(default=False)
       address = models.GenericIPAddressField()
       admin_contact = models.EmailField(max_length=70, null=True, blank=True)
       admin_phone = models.CharField(max_length=70, null=True, blank=True, default='')

       def __repr__(self):
           return "<{} address: {}>".format(self.__class__.__name__, self.address)

       def __str__(self):
           return "{} o adresie: {}".format(self.__class__.__name__, self.address)

Utworzenie oraz uruchomienie migracji
--------------------------------------

.. code-block:: bash

   python manage.py makemigrations
   python manage.py migrate

Sprawdzenie kodu sql migracji
------------------------------

.. code-block:: bash

   python manage.py sqlmigrate api 0001

Odpalenie testu po dodaniu modelu
----------------------------------

.. code-block:: bash

   python manage.py test

Dodanie testu widoku
--------------------

.. code-block:: python

   # api/tests.py
   # ............

   from django.test import TestCase
   from rest_framework import status
   from api.models import Server
   from django.urls import reverse

   class ViewServerTestCase(TestCase):
       """Test dla widoku serwera"""

       def test_create_server(self):
           """Sprawdzimy czy mozna utworzyc serwer (post)"""

           url = reverse('servers')
           data = {"location": "office", "address": "127.0.0.1"}

           response = self.client.post(url, data, format="json")

           self.assertEqual(response.status_code, status.HTTP_201_CREATED)
           self.assertEqual(len(response.data), 1)

       def test_view_server_list(self):
           """Sprawdzimy czy pobierze wlasciwa ilosc serverow"""

           url = reverse('servers')

           response = self.client.get(url, format="json")
           self.assertEqual(response.status_code, status.HTTP_200_OK)


Serializer
-----------

.. code-block:: bash

   # api/serializers.py

   from rest_framework import serializers

   from .models import Server

   class ServerSerializer(serializers.ModelSerializer):
       class Meta:
           model = Server
           fields = '__all__' # albo fields = ('location', 'address',)


Dodanie widoku
--------------------

.. code-block:: bash

   # api/views.py

   from rest_framework.views import APIView
   from rest_framework.response import Response
   from rest_framework import status
   
   from .serializers import ServerSerializer
   from .models import Server
   
   class ServerList(APIView):
       """Lista serwerow"""
   
       serializer_class = ServerSerializer
   
       def get_queryset(self):
           queryset = Server.objects.all()
           location = self.request.query_params.get('location', None)

           if location is not None:
               queryset = queryset.filter(location__icontains=location)
           return queryset

       def get(self, request, format=None):

           servers = self.get_queryset()
           serializer = ServerSerializer(servers, many=True)
           return Response(serializer.data)
   
       def post(self, request, format=None):
           serializer = ServerSerializer(data=request.data)
   
           if serializer.is_valid():
               serializer.save()
               return Response(serializer.data, status=status.HTTP_201_CREATED)
           return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


Dodanie urls
-------------

.. code-block:: python

   from django.contrib import admin
   from django.urls import path

   from api import views

   urlpatterns = [
       path('admin/', admin.site.urls),
       path('servers/', views.ServerList.as_view(), name='servers')
   ]


Metody
------

* Get

* Post

* Put

* Delete        

Kody HTTP
---------

* ``200`` - powodzenie. Request poprawy. Odpowiedź poprawna.
* ``400`` - zły **request**. Źle sformuowany request / problemy z autentykacją.
* ``403`` - dostęp zabroniony,
* ``404`` - nie ma takiej strony,
* ``500`` - błąd wewnętrzny serwera. Najczęściej gdy popełniliśmy jakiś błąd w kodzie np. w django.


Requesty
--------


Odpowiedzi (response)
---------------------

Settingsy
---------



Widok
-----

Migracja
--------

.. hint::

   Aby podejrzeć migracje można użyć:
   ``python manage.py showmigrations``

Orm
---

Django extensions
------------------

.. code-block:: python

   python manage.py show_urls
   python manage.py shell_plus # lepsza konsola - ipython
   python manage.py runserver_plus # server 

Zadania
-------

Zero
____

* Napraw test,
* Dodaj do testu sprawdzanie daty - stworzenia wpisu

Pierwsze
________

* Stworz 4 hosty - każdy w inny sposób

  * Przez stronę web,
  * Przez ``request`` z postmana_,

.. _postmana: https://www.getpostman.com/
  * Przez ``python manage.p shell`` lub ``shell_plus``,
  * Przez insert do bazy danych

* Stwórz model, który:

  * Będzie przetrzymywał ``datę``


Pierwsze
________

* Stwórz endpoint (POST), który:

  * Będzie

Drugie
______

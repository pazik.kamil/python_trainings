Drzewo
====================



Definicja drzewa
====================

.. code-block:: python
   :linenos:

   class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None
    def insert(self, value):
        if value <= self.value:
            if self.left is None:
                self.left = Node(value)
            else:
                self.left.insert(value)
        else:
            if self.right is None:
                self.right = Node(value)
            else:
                self.right.insert(value)
    def __repr__(self):
        return '<Node: value: {}'.format(str(self.value))
    def search(self, value):
        if self.value == value:
            return True
        elif value < self.value:
            if isinstance(self.left, type(None)):
                return False
            if self.left.value == None:
                return False
            else:
                return self.left.search(value)
        else:
            if isinstance(self.right, type(None)):
                return False
            if self.right.value == None:
                return False
            else:
                return self.right.search(value)

    def __contains__(self, value):
        return self.search(value)


    def in_order_traversal(self):
        if self.left != None:
            self.left.in_order_traversal()
        print(self.value)

        if self.right != None:
            self.right.in_order_traversal()
    def get_height(self):
        height = 1
        if not isinstance(self.left, type(None)):
            height_left = self.left.get_height()
        else:
            height_left = 0
        if not isinstance(self.right, type(None)):
            height_right = self.right.get_height()
        else:
            height_right = 0

        return height + max(height_left, height_right)
    def __len__(self):
        return self.get_height()

    a = Node(6)
    len(a)


.. hint::

   tree


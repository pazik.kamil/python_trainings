.. Python kurs documentation master file, created by
   sphinx-quickstart on Fri Jan 25 22:42:03 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



*****
Wstęp
*****
.. toctree::
    :maxdepth: 3 
    :numbered:
    :caption: Wstęp
    :name: intro

    intro/intro
    intro/installation
    intro/python_env
    intro/editors
    intro/ide
    intro/python
    intro/shells
    intro/naming
    intro/pep8


********
Podstawy
********
.. toctree::
    :maxdepth: 3
    :numbered:
    :name: basic
    :caption: Podstawy

    basic/basic
    basic/strings
    basic/integers
    basic/floats
    basic/lists
    basic/dicts
    basic/sets

**************************
Sterowanie wykonaniem kodu
**************************
.. toctree::
    :maxdepth: 3
    :numbered:
    :name: flowcontrol
    :caption: Sterowanie wykonaniem

    control-flow/ifelse
    control-flow/loops
    control-flow/comprehensions
    control-flow/functions
    control-flow/exceptions
    control-flow/iterators
    control-flow/generators

**************************
Biblioteka standardowa
**************************
.. toctree::
    :maxdepth: 3
    :numbered:
    :name: stdlibs
    :caption: Biblioteka standardowa

    stdlibs/pickling
    stdlibs/files
    stdlibs/regex

**************************
Programowanie obiektowe
**************************
.. toctree::
    :maxdepth: 3
    :numbered:
    :name: oop
    :caption: Programowanie obiektowe

    oop/basics

**************************
Programowanie sieciowe
**************************
.. toctree::
    :maxdepth: 3
    :numbered:
    :name: network-programming
    :caption: Programowanie sieciowe

    networking/webapi
    networking/rest
    networking/database
    networking/sockets

**************************
Testowanie
***************************
.. toctree::
    :maxdepth: 3
    :numbered:
    :name: testing
    :caption: Testowanie

    testing/unit

**************************
Programowanie zaawansowane
**************************
.. toctree::
    :maxdepth: 3
    :numbered:
    :name: advanced
    :caption: programowanie zaawansowane

    advanced/decorators
    advanced/proxy
    advanced/hash


**************************
Python internals
**************************
.. toctree::
    :maxdepth: 3
    :numbered:
    :name: python_internals
    :caption: python internals

    internals/compiling
    internals/oslib
    internals/range
    internals/language


**************************
Algorytmy
**************************
.. toctree::
    :maxdepth: 3
    :numbered:
    :name: algorytmy
    :caption: algorytmy

    algo/tree
    algo/linked
    algo/linkedlist_notebook
    algo/stack

*******
Kontakt
*******
.. toctree::
    :maxdepth: 3
    :name: contact

    contact

Indeks i tablice 
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Programowanie obiektowe
=========================

Zadania
---------------------------

Pierwsze
_______________

* Stworz klase pojazd_mechaniczny, który będzie dziedziczyl po pojazdach,
* Przy tworzeniu pojazdu mech. Pytamy o numer VIN
* Dodaj właściwości:
* Ilosc paliwa
* Spalanie na 100 km
* Dodaj własciwosc (property)
* Dodaj metode liczaca spalanie na 100 km na MPG (statyczna metoda)

Drugie
_______

* Stworz klase Server posiadajaca:

  * Nazwe,
  * Ip,
  * Uzyj importu os,
  * Stworz metode pinguj (uzyj os – opal ping systemowy),
  * Zapisuj historie pingów - data i czy sie udalo,
  * Stworz liste hostow do pingowania [ ‘127.0.0.1’, …..],
  * Wyswietl komunikat czy się udalo zrobic pinga

.. hint::

   Biblioteka __pathlib__ (std). Klasa `PurePath_:

   .. _PurePath: https://github.com/python/cpython/blob/35d9c37e271c35b87d64cc7422600e573f3ee244/Lib/pathlib.py#L1007

   

.. hint::


   Biblioteka `ldap3_

   .. _ldap3: https://github.com/cannatag/ldap3/blob/2d19835dfe55247647d3334212efd13dd0899451/ldap3/core/server.py#L52

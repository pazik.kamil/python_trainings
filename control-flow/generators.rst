Generatory
=============================

* Lazy evaluation,
* Oszczędne pamięciowo


.. testcode::

   import collections, types

   print(issubclass(types.GeneratorType, collections.Iterator))


.. testoutput::

   True


.. note::

   Generator jest **Iteratorem**, lecz **Iterator** nie jest **Generatorem** !


Wyrażenie jako generator
------------------------

.. code-block:: python

   g = (n for n in range(20) if not n % 2) # tylko parzyste !

   for x in g:
       print(x)

Funkcja jako generator
-----------------------

.. testcode::

   def simple_gen():
       yield 5
       yield 10
       yield 15

   s = simple_gen()

   print(next(s))
   print(next(s))
   print(next(s))

.. testoutput::

   5
   10
   15

.. code-block:: python

   # już nie ma elementów aby przeiterować
   print(next(s))

   StopIteration:

.. testcode::

   def new_range(start=0, stop=None, step=1): 
       i = start

       if stop is None:
           while True:
               yield i
               i += step
       else:
           while i < stop:
               yield i
               i += step
    
   g = new_range(2, 5)

   print(next(g))
   print(next(g))

.. testoutput::

   2
   3

.. note::

   Generatory to funkcje produkujace kolejne wartosci. Gdy iteratory to obiekty używające metody ``next()``

Zadania
-------

Pierwsze
________

* Utwórz generator, który generuje wartości będące **3 krotnością** wartości od 0 do 20

Trzecie
________

* Wykorzystując plik_ z zadnia z **list comprehension** 

.. _plik: http://python.variantcore.com/lista_dat.txt

* Ściągnij plik przy użyciu pythona 


.. hint::

   * Można użyć biblioteki: ``urllib``
   * Można użyć domyślnej funcki ``open`` oraz ``readline``

* Oczyść plik,
* Napisz generator konwertujący datę w tekscie na format datowy 
* Jako, że w trakcie generowania logów mieliśmy źle ustawiony zegar - musimy dodać jedną godzinę

.. hint::

   W pakiecie ``datetime`` jest ``timedelta``


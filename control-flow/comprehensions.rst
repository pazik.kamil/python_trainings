List/Dict/Set comprehensions
=============================
Używa się w celu zwiększenia czytelności kodu.

.. hint::

   Z początku warto napisać wersję linniową, dopiero później przejść na "comprehension"

.. testcode::

   lista_parzystych_comprehension = [element for element in range(2, 21, 2)]
   print(lista_parzystych_comprehension) 

.. testoutput::

   [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
   

.. testcode::

   lista_parzystych_comprehension2 = [element for element in range(2, 21) if (element % 2) == 0 ]
   print(lista_parzystych_comprehension2) 

.. testoutput::

   [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]

.. testcode::

   lista_parzystych_comprehension3 = [element for element in range(2, 21) if not (element % 2)]
   print(lista_parzystych_comprehension3) 

.. testoutput::

   [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]

Dict comprehension
------------------

.. testcode::

   slownik = {'Adam': 'Audi', 'Tomek': 'BMW', 'Kasia': 'Citroen'} # doctest: +SKIP

Set comprehension
-----------------

.. testcode::

   zbior = {i**2 for i in range(5)}
   print(zbior)

.. testoutput::

   {0, 1, 4, 9, 16}

Zadania
-------

Pierwsze
________

* Znajdz 20 liczb podzielnych jednocześnie przez 2 i 5

Drugie
______

* Stwórz mapowanie (dict comprehension)
  
  * klucz to liczba, wartość to kolejne litery z alfabetu,
  * ``{0: 'A', 1: 'B', 2: 'C', 3: 'D'}``

.. hint::

   Spójrz na tablicę ASCII

   * Da się konwertować liczbę na literę,
   * da się użyć funkci ``chr()`` **google**

Trzecie
________

* mając **listę dat** w pliku_

.. _pliku: http://python.variantcore.com/lista_dat.txt

  * Przetwórz plik wydobywająć jedynie czas (list comprehension)
    * na podstawie tej listy - utwórz listę, gdzie zdarzenie nastąpiło w 19 sekundzie


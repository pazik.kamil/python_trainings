Funkcje
==========================

* Daje możliwośc reużywalności kodu,
* Ułatwia śledzenie jego używania,
* Bardziej logiczna struktura kodu niz linnia po linni


Poniżej definicja funkcji

.. testcode::
  
   def funkcja():
       """"Docstring dokuentujacy funkcje - definicja"""

       print('To jest funkcja')

   # "Uruchomienie" funkcji
   funkcja()

.. testoutput::

   To jest funkcja

Funkcja z parametrami
---------------------

.. testcode::

   def suma_trzech_liczb(a, b, c):
       """Prosta funcka zliczająca 3 liczby"""

       print(a + b + c)
  
   wynik = suma_trzech_liczb(3, 5, 8)
   print(wynik)

.. testoutput::

   16
   None

.. testcode::

   def suma_czterech_liczb(a, b, c=0, d=0):
       """Prosta funkcja zliczająca 4 liczby"""

       return (a + b + c + d)
  
   print(suma_czterech_liczb(3, 5))
   print(suma_czterech_liczb(3, 5, 8))
   print(suma_czterech_liczb(3, 5, 8, 16))

.. testoutput::

   8
   16 
   32

Args
----
 
.. testcode::

   def suma_wielu_liczb(wyswietl, *liczby):
       suma = 0

       for liczba in liczby:
           suma += liczba

       if wyswietl:
           print('Suma wynosi {}'.format(suma))
       return suma

   wynik = suma_wielu_liczb(True, 1, 2, 3, 4, 5, 6, 7)
   print(wynik)

.. testoutput::

   Suma wynosi 28 
   28


Kwargs
-------

.. testcode::

   def suma_zarobkow(**kwargs):
       """sumuje wszystkie osoby"""

       suma = 0

       for czlowiek, zarobki in kwargs.items():
           suma += zarobki
       return suma

   print(suma_zarobkow(Adam=3000, Tomek=2500, Kasia=4320))

.. testoutput::

   9820

Zadania
-------

Pierwsze
________
* Nie użytkownik poda swój wiek, sprawdź czy jest pełnoletni,
* Niech użytkownik poda liczbę, sprawdź czy liczba ta jest int czy float

Drugie
______
* Stwórz prostą funkcję która będzie sprawdzała siłę hasła (własny algorytm)
* Hasło może być przynajmniej 6 literowe, maksymalnie 9 
* Hasło jest tyle mocniejsze, gdy:

  * Posiada wielkie litery,
  * Posiada liczbę,
  * Posiada znak specjalny (możesz sam zdefiniować listę specjalną np. ``['_', '*', '&']``

Trzecie
______________________________
* Zmodyfikuj kod na liczenie BMI - tak aby była to teraz funkcja, która dodatkowo przyjmuje 

  * Imię,

Czwarte
________

* Napisz funkcje ``dane_zarobkow(dzial, statystyki=True, *args)`` która dla podanego dzialu zwróci średnią zarobków działu - zaokrąglij do pełnych złotówek
* Dodatkowo gdy flaga statystyki jest wlączona funkcja opisze więcej statystyk:

  * Średnią,
  * Medianę (własna funkcja),
  * Minimalna wartosc,
  * Maksymalną wartość

.. hint::

   W celu obliczenia mediany można napisać funkcję.
   Można także wykorzystać rozwiązanie z bibliotek

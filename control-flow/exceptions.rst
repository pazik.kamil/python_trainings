Wyjątki
==========================

* W przypadku wykonania niedozwolonej operacji,
* W sytuacjach gdy zasób jest dla nas niedostępny - np. niewystarczające uprawnienia / za mało pamięci itp.
  

Syntax Errors
-------------

.. code-block:: python

   >>> while True print('Hello world')
   File "<stdin>", line 1
   while True print('Hello world')
                   ^
   SyntaxError: invalid syntax


Key Errors
----------

.. code-block:: python

   stolice = {"Francja": "Paryz", "Niemcy":"Berlin", "Polska":"Warszawa", "Czechy":"Praga"}
   stolice["USA"]

   KeyError: 'USA'

Attribute error
---------------

* Gdy operacja niedostępna 

.. code-block:: python

   "Hello Wordl".append('!')


Indentation Error
-----------------

.. code-block:: python

   def testfunc():
   print('Hello ;)')
    print('My name is:')

   File "<ipython-input-4-9cd3c6fb52a1>", line 3
    print('My name is:')
    ^
   IndentationError: unexpected indent

ModuleNotFoundError
--------------------

.. code-block:: python

   import not_existing_module

   ModuleNotFoundError: No module named 'not_existing_module'

Tablica hierarchi wyjątków w Python_.

.. _Python: https://docs.python.org/3/library/exceptions.html#exception-hierarchy

IndexError
----------

.. code-block:: python

   uczestnicy = ['Kasia', 'Adam', 'Tomek']
   uczestnicy[6]

   IndexError: list index out of range

Obsługa wyjątków
----------------

.. testcode::

   for i in range(3, -3, -1):
    try:
        print('Próba dzielenia przez {}'.format(i))
        3 / i
    except ZeroDivisionError:
        print('Pomijam, nielegalna operajca !!!')

    finally:
        print('Koniec obsługi')

.. testoutput::

   Próba dzielenia przez 3
   Koniec obsługi
   Próba dzielenia przez 2
   Koniec obsługi
   Próba dzielenia przez 1
   Koniec obsługi
   Próba dzielenia przez 0
   Pomijam, nielegalna operajca !!!
   Koniec obsługi
   Próba dzielenia przez -1
   Koniec obsługi
   Próba dzielenia przez -2
   Koniec obsługi

Podniesienie wyjątku
--------------------

.. code-block:: python

   def generate_report(input_data, outputfile):
       raise NotImplementedError('Function development still in progress')

   NotImplementedError: Function development still in progress

Zadania
-------

Pierwsze
________

* Masz listę uczestników 

  * uczestnicy = ["Kasia", "Adam", "Tomek"]

* Obsłuż sytację w której

  * Wydobywany jest elemnt z listy o indeksie 5

  * Nastąpi



 

Obsłuż wy

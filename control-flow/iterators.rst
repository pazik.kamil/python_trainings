Iteratory
=============================

* Lazy evaluation,
* Oszczędne pamięciowo,
* Używane są w wielu miejscach

  * open,
  * zip,
  * enumerate,
  * reversed

.. testcode::
  
   from typing import Iterable
   print(issubclass(range, Iterable))


.. testoutput::

   True

.. hint::

   Podobnie można sprawdzić inne typy tj. list, string

.. testcode::

   from typing import Iterable, Iterator

   print(isinstance(range(10), Iterable))
   print(hasattr(range(10),'__iter__'))
   print(callable(range(10).__iter__))
   print(isinstance(iter([1,2]) , Iterator))

.. testoutput::

   True
   True
   True
   True

Iteratoru vs listy
-------------------

.. code-block:: python

   # nie zużywa pamięci - iteruje w locie
   for i in ( i ** 2 for i in range(10**8)):
       print(i)


.. code-block:: python

   # zużywa dużo pamięci

   lista = [ i ** 2 for i in range(10**8)]

.. hint::

   Porównaj procesy dla listy oraz generatora przy użyciu ``ps aux PID``
   Dodatkowo możesz użyć funckji linuxowej ``watch -d -n 0.1``

Definiowanie iteratorów
-----------------------

.. testcode::

   class Numbers:
       def __iter__(self):
           self.value = 1
           return self

       def __next__(self):
           value = self.value
           self.value += 1
           return value

   numbers = Numbers() 
   my_iter = iter(numbers)

   print(next(my_iter))
   print(next(my_iter))
   print(next(my_iter))

.. testoutput::

   1
   2
   3

Zip
----

  .. testcode::

     from typing import Iterable, Iterator

     za = zip([1,2,3], ['a', 'b', 'c'])
     print(isinstance(za, Iterable))
     print(isinstance(za, Iterator))

  .. testoutput::

     True
     True

Zadania
--------

Pierwsze
________

* Mamy listę wydatki w poszczególnych dniach tygodnia

  * wydatki = [11.25, 18.0, 20.0, 10.75, 9.50]

* Wypisz (bez użycia ``range`` / ``len``) **google** 

  * "Koszt parkingu nr 1: 11.25" 
  * "Koszt parkingu nr 2: 18.00" 

.. hint::

   Można wykorzystać słowo kluczowe enumerate

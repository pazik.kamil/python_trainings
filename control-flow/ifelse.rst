Instrukcje warunkowe
==========================

* if-y

.. testcode::

   if True:
       print('Wartość prawdziwa')

Powyższy kod zwraca tekst:

.. testoutput::

   Wartość prawdziwa


.. testcode::

   if False:
       print('Wartość fałszywa')

Konwersje typów do Boolean
---------------------------

.. testcode::

   if []:
       print('Lista z zawartoscia')
   else:
       print('Lista pusta')

Powyższy kod zwraca tekst:

.. testoutput::

   Lista pusta

Sprawdzanie wartości logicznych
--------------------------------

>>> bool(-1)
True

>>> bool(0)
False

>>> bool(124)
True

>>> bool({})
False

Sprawdzanie zakresów
--------------------

.. testcode::

   temperatura = 18

   if 16 <= temperatura < 24:
       print('Temperatura dobra na rower')
   else:
       print('Temperatura nieodpowiednia na jazdę rowerem')

.. testoutput::

   Temperatura dobra na rower



.. testcode::

   temperatura = -3 

   if 16 <= temperatura < 24:
       print('Temperatura dobra na rower')
   elif 3 <= temperatura < 16:
       print('Temperatura dobra na spacer')
   elif -5 <= temperatura < 3:
       print('Temperatura dobra na narty')
   else:
       print('Niewiadomo co robić :(')

.. testoutput::

   Temperatura dobra na narty

Zadania
-------

Pierwsze
________
* Nie użytkownik poda swój wiek, sprawdź czy jest pełnoletni,
* Niech użytkownik poda liczbę, sprawdź czy liczba ta jest int czy float
 
Drugie
______
* Stwórz prosty kalkulator BMI który będzie przyjmował wartości z ``input`` w wyniku będzie zwracał status czy masz nadwagę, niedowagę czy w normie

Trzecie
_______
* Użyj biblioteki ``os`` funkcji ``system`` aby sprawdzić czy host jest aktywny 

  * W zależności od stanu wyświetl stosowny komunikat,

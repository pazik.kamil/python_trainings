Pętle
==========================

for
---


.. testcode::

   for i in range(5):
       print(i)

.. testoutput::

   0
   1
   2
   3
   4

.. testcode::

   for i in range(2, 6):
       print(i)

.. testoutput::

   2
   3
   4
   5

.. testcode::

   for i in range(2, 7, 2):
       print(i)

.. testoutput::

   2
   4
   6

while
-----

.. testcode::

   # i to nasz iterator
   i = 0
   while i < 10:
       print(i)
       i += 1

.. testoutput::

   0
   1
   2
   3
   4
   5
   6
   7
   8
   9

.. testcode::

   # i to nasz iterator
   i = 6
   while i < 10:
       if i == 7:
           print('Szczęśliwa 7ka')
           i += 1
           continue

       print(i)

       i += 1

.. testoutput::

   6
   Szczęśliwa 7ka
   8
   9


.. testcode::

   # i to nasz iterator
   i = 6
   while i < 10:
       if i == 7:
           print('Szczęśliwa 7ka ?')
           break

       print(i)

       i += 1

.. testoutput::

   6
   Szczęśliwa 7ka ?

iterowanie po iterables
-----------------------

.. testcode::

   for samochod in ['BMW', 'Audi', 'Mercedes']:
       print(samochod)

.. testoutput::

   BMW
   Audi
   Mercedes


Zadania
-------

Pierwsze
________
  * Utwórz ``słownik hostów`` których zapiszesz datę wykonania "skanowania" oraz status czy się powiodło

    * Możesz zdefiniować listę hostów np. ``wp.pl, google.com, ing.pl, nieistniejacyhost.domena``
    * Możesz użyć biblioteki ``datetime`` **google**
    * Spróbuj zczytać datę dostępną w systemie (os.popen)

Drugie
______
  * Utwórz listę parzystych liczb z zakresu 0, 100,
  * Wyświetl listę

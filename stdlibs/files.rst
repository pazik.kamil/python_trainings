Operacje plikowe
=============================

Odczytywanie plików
--------------------

.. code-block:: python

   file = 'plik.txt'



Czytanie linnia po linni
-------------------------


.. code-block:: python

   with open(r'../plik.txt') as plik_deskryptor:
       lines = deskryptor_pliku.readlines()


Zapis
-----

.. code-block:: python

   with open(r'/tmp/iris.csv', mode='w') as plik_deskryptor:
       plik_deskryptor.write('hello')

Context manager
---------------

.. code-block:: python

   with open(r'plik.txt') as deskryptor_pliku:
        for linia in deskryptor_pliku:
            print(linia)

Wyrażenia regularne
=============================

Import
------

>>> import re

Zastosowania
-------------

* Przetwarzanie tekstu,

  * Znajdowanie wzorców, 
  * Oczyszczanie danych

* Walidacja poprawności danych

Funkcje w pakiecie re
----------------------

+------------+------------------------------------------------------------+------------+ 
| funkcja    | znaczenie oraz użycie                                      | Wynik      |
+============+============================================================+============+
| re.match   | Czy pasuje   ``re.match(r"(\d+)\.(\d+)", "24.1632")``      | True/False |
+------------+------------------------------------------------------------+------------+ 
| re.search  | Pierwsze położenie ``re.search('(?<=abc)def', 'abcdef')``  |            | 
+------------+------------------------------------------------------------+------------+
| re.split   | Dzieli po seperatorze ``re.split(r'\W+', 'a, b, c')``      | Lista      | 
+------------+------------------------------------------------------------+------------+
| re.findall | Znajdź wszystkie wystąpienia ``re.findall('\w+', "A B")``  | Lista      | 
+------------+------------------------------------------------------------+------------+
| re.finiter | Znajdź wszystkie wystąpienia ``re.finditer('\w+', "A B")`` | Iterator   | 
+------------+------------------------------------------------------------+------------+

Klasy znaków
------------


+------------+------------------------------------------------------------+------------+ 
| klasa      | znaczenie                                                  |            |
+============+============================================================+============+
|     .      | Dowolny znak                                               |            |
+------------+------------------------------------------------------------+------------+ 
|     ^      | Początek linni                                             |            | 
+------------+------------------------------------------------------------+------------+ 
|     $      | Koniec linni                                               |            | 
+------------+------------------------------------------------------------+------------+ 
|     *      | Zero lub więcej wystąpień                                  |            | 
+------------+------------------------------------------------------------+------------+
|     +      | Jeden lub więcej wystąpień                                 |            | 
+------------+------------------------------------------------------------+------------+
|     ?      | Jeden lub zero wystąpień                                   |            | 
+------------+------------------------------------------------------------+------------+
|    {n}     | N wystąpięń                                                |            | 
+------------+------------------------------------------------------------+------------+
|   {n, m}   | Ilość wystąpięń w przedziale od ``n`` do ``m``             |            | 
+------------+------------------------------------------------------------+------------+
|   \d       | Grupa liczbowa synonim [0-9]                               |            | 
+------------+------------------------------------------------------------+------------+
|   \D       | Anty grupa liczbowa synonim [^0-9]                         |            | 
+------------+------------------------------------------------------------+------------+
|   \w       | Grupa "słowna" - synonim [a-zA-Z0-9_]                      |            | 
+------------+------------------------------------------------------------+------------+
|   \W       | Anty grupa "słowna" - synonim [^a-zA-Z0-9_]                |            | 
+------------+------------------------------------------------------------+------------+
|   \s       | Grupa białych znaków - synonim [\r\n\t\f\v]                |            | 
+------------+------------------------------------------------------------+------------+
|   [abc]    | Grupa znaków a, b lub c                                    |            | 
+------------+------------------------------------------------------------+------------+
|   [a-z]    | Znaki w zakresie od ``a`` do ``z``                         |            | 
+------------+------------------------------------------------------------+------------+
|     ()     | Grupa                                                      |            | 
+------------+------------------------------------------------------------+------------+


Zadania
-------

Pierwsze
________

* Stwórz funkcję ``sprawdz_ip``

  * Funkcja będzie sprawdzać czy IP jest poprawne,
  * Przetestuj funkcję na slowniku hostow 

.. code-block:: python     

   {
       '127.0.0.1': {'poprawny': None},
       '8.8.8.8': {'poprawny': None},
       'x.x.x.x': {'poprawny': None}
   }

  * W miejsce ``x.x.x.x`` wstaw adres hosta w swojej sieci,
  * Zaktualizuj flagę ``poprawny``


.. hint::

   Można użyć poniższego wyrażenia regularnego, lub znaleźć / stworzyć bardziej dokładne wyrażenie
   ``^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$``

Drugie 
_______

 * Stwórz funkcję ``sprawdz_email``

   * Funkcja będzie sprawdzać czy email jest poprawny

Trzecie
_______

 * Używając biblioteki ``requests``

   * Ściągnij zawartość strony_

.. _strony: https://python-trainings.readthedocs.io/en/latest/control-flow/loops.html

   * Wydobądź wszystkie tagi **html**,
   * Wydobądź słowa - które człowiek jest w stanie przeczytać

Czwarte
_______

 * Używając biblioteki ``collections``

   * Wylicz wystąpienia słów z punktu Trzeciego,
   * Wyświetl top 10 najczęstszych słów - wnioski ?,
   * Wyświetl top 70 najczęstszych słów - wnioski ?
   
Zasoby
------

* Regex Expressions 101 - strona_

.. _strona: https://regex101.com/

* Regular Expressions Cookbook by Steven Levithan, Jan Goyvaerts - książka_

.. _książka: https://www.oreilly.com/library/view/regular-expressions-cookbook/9780596802837/ch07s16.html

* Stack overflow disscussion_

.. _disscussion: https://stackoverflow.com/questions/106179/regular-expression-to-match-dns-hostname-or-ip-address

* PyRegex_ 
  
.. _PyRegex: http://www.pyregex.com/

Biblioteka standardowa
=============================

Pickling
--------
Proces serializacji danych binarnych do postaci pliku

Dump
----

.. code-block:: python

   godziny = ['Tue Mar 29 23:40:17 +0000 2016', 'Tue Mar 29 23:40:19 +0000 2016']

   plik_zapis = open('/Users/kamil/daty.pickle', 'wb') # write/binary
   pickle.dump(godziny, pliczek)

Load
----

.. Hint::

   Metoda ``load`` zamiast dump

.. Warning::

   W przypadku **Linuxa** lub **Windows** są inne ścieżki.
   

Zadania
-------

Pierwsze
________

* Ściągnąć podany  plik_

.. _plik: http://python.variantcore.com/daty.pickle

* Przetworzyć daty (stringi) do formatu z paczki datetime, w przypadku gdy 

.. hint::

   Aby ustalić format można zajrzeć do dokumentacji_

.. _dokumentacji:

  https://docs.python.org/3/library/datetime.html

* Zapisać pickle po przetworzeniu
* Odczytać pickle do innej zmiennej - sprawdzić
